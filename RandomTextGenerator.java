// Name:Alejandro Monroy Reyes
// USC loginid: monroyre
// CS 455 PA4
// Fall 2016

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Scanner;
import java.util.Random;
import java.io.PrintWriter;
import java.lang.IllegalArgumentException;

/**
RandomTextGenerator class
*Class to generate random text.
*/

public class RandomTextGenerator {
    private Map<Prefix, ArrayList<String>> prefixDictionary;
    private ArrayList<String> wordsDictionary;
    private int prefixLength;
    private boolean debug;
    private Scanner in;
    private Random rand; 
    
    /**
    Constructs the RandomTextGenerator.
    @param in: Scanner initialized to the input file. 
    @param prefixLength: size of the prefix.
    @param debug: debug on/off flag.
    @throws IllegalArgumentException: If number of words in sourceFile is <= to prefixLength
    */
    public RandomTextGenerator(Scanner in, int prefixLength, boolean debug) throws IllegalArgumentException
    {
        this.debug = debug;
        this.in = in;
        this.prefixLength = prefixLength;
        prefixDictionary = createPrefixSucesorsMap(in, prefixLength);
        if(debug){rand = new Random(1);}
        else{rand = new Random();} 
    }
    
    /**
    Creates a prefix dictionary:
    @param in: Scanner initialized to the input file. 
    @param prefixLength: size of the prefix.
    @return prefixDicitionary: return a Map<Prefix,ArrayList<String>> containing prefixes and sucesors. 
    @throws IllegalArgumentException: If number of words in sourceFile is <= to prefixLength
    */
    private Map<Prefix, ArrayList<String>> createPrefixSucesorsMap(Scanner in, int prefixLength) throws IllegalArgumentException
    {
        wordsDictionary= new ArrayList<String>();
        while(in.hasNext())
        {
            wordsDictionary.add(in.next());
        }
        if(wordsDictionary.size () <= prefixLength)
        {
            throw new IllegalArgumentException("Ilegal Input File Size: number of words in sourceFile must be > prefixLength");
        }
        
        Map<Prefix, ArrayList<String>> prefixDictionary = new HashMap<Prefix,ArrayList<String>>();          
        for(int i =0; i <=wordsDictionary.size()-prefixLength; i++)
        {
            LinkedList<String> prefixWords = new LinkedList<String>();
            for(int j =0; j <prefixLength; j++)
            {
                prefixWords.add(wordsDictionary.get(i+j)); 
            }
                
            Prefix prefix = new Prefix(prefixWords);
            
            if(!prefixDictionary.containsKey(prefix))
            {
                prefixDictionary.put(prefix, new ArrayList<String>());
                ArrayList<String> temp = prefixDictionary.get(prefix);
                if(i+prefixLength < wordsDictionary.size())
                {
                    temp.add(wordsDictionary.get(i+prefixLength));
                }
            }
            else
            { 
                if(i+prefixLength < wordsDictionary.size())
                {
                    ArrayList<String> temp = prefixDictionary.get(prefix);
                    temp.add(wordsDictionary.get(i+prefixLength));
                }
            }
        }
        return prefixDictionary;
    }
      
    /**
    Process and generate the output to an out file. 
    @param words: Number of words to be generated.
    @param writer: PrintWriter initialized to the output file. 
    */
    public void generateText(int words, PrintWriter writer)
    {   
        String text = "" ;
        Prefix startPrefix = getRandomPrefix();
        for (int i = 0; i <words; i++)
        {
            while(getSuccesor(startPrefix).equals("<END OF FILE>"))
            {
                if(debug){System.out.println("DEBUG: Succesors: <END OF FILE>");}
                startPrefix = getRandomPrefix();
            }
            String succesor= getSuccesor(startPrefix);
            text = text + succesor + " ";
            if(debug){System.out.println("DEBUG: Succesors: " + prefixDictionary.get(startPrefix).toString());}
            if(debug){System.out.println("DEBUG: Word Generated:" + succesor);}
            
            startPrefix = startPrefix.shift(startPrefix, succesor);
            if(debug){System.out.println("DEBUG: Prefix: " + startPrefix);}
        }
        
        processOutput(text, writer);     
    }

    /**
    Get a random prefix from the words dictionary.
    @return the generated Prefix object. 
    */
    private Prefix getRandomPrefix() {
        int value = rand.nextInt(wordsDictionary.size() - prefixLength);
        LinkedList<String> randomPrefixList = new LinkedList<String>();
        for( int i =0; i< prefixLength; i++)
        {
            randomPrefixList.add(wordsDictionary.get(value+i));
        }
        Prefix randomPrefix = new Prefix(randomPrefixList);
        if(debug){System.out.println("DEBUG: chose a new initial prefix: " + randomPrefix);}
        return randomPrefix;
    }
    
    /**
    Return a random sucesor from a given Prefix;
    @param startPrefix a Prefix object.
    @return the selected random sucesor. 
    */
    private String getSuccesor(Prefix startPrefix)
    {
        ArrayList<String> arr = prefixDictionary.get(startPrefix);
        if(arr.size() == 0)
        {
            return "<END OF FILE>";
        }
        return arr.get(rand.nextInt(arr.size()));
    }
    
    /**
    Processes the generated words and write them to the file in a format;
    @param writer initialized to the output file.  
    @param text containing the generated words to be processed. 
    */
    private void processOutput(String text, PrintWriter writer)
    {
        in = new Scanner(text);
        String line = "";
        while(in.hasNext())
        {
            String nextWord = in.next();
            if((line.length() + nextWord.length()) <=80)
            {
                line = line + nextWord + " ";
            }
            else
            {
                writer.println(line.substring(0, line.length()-1));
                line = nextWord + " ";
            }
        }
        if(line.length()>0)
        {
            writer.print(line.substring(0, line.length()-1));
        }
    }
}

