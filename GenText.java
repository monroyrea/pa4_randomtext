// Name:Alejandro Monroy Reyes
// USC loginid: monroyre
// CS 455 PA4
// Fall 2016

import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

/**
GenText class
*
* Responsible class for processing following command-line arguments.
*   -[-d] optional argument, if included will turn on debug mode, otherwise debug mode will be off.
*   -int prefixLength: prefixLength order of the words.(prefixLength > 1)
*   -int numWords: number of words to be generated. (numWords > 0)
*   -String sourceFile: input file that contains n words to generate the output. (number of words in source file > prefixLength)
*   -String outFil: file where the words will be written. 
*
*The class also handles errors related to command line arguments and input/output files.
*/

public class GenText {
    public static void main(String[] args) throws FileNotFoundException
    {
        boolean debug = false;  //Flag to turn on/off the debugger mode.
        int prefixLenght = 0;
        int numWords = 0;        
        try
        {
            if(args.length<4)
            {
                throw new IllegalArgumentException("\nMissing command-line arguments. Arguments must be more than 4: [-d] prefixLenght, numWords, sourceFile, outFile.\n" + printManual());
            }
            else if(args.length > 4)
            {
                if(args[0].equals("-d")){debug = true;}
                for(int i =0; i< args.length-1; i ++)
                {
                    args[i] = args[i+1];
                }
            }
            prefixLenght = Integer.parseInt(args[0]);      
            numWords = Integer.parseInt(args[1]); 
            if(prefixLenght<1)
            {
                throw new IllegalArgumentException("Illegal prefixLenght value: prefixLength must be greater than 0.\n" + printManual()); 
            }
            if(numWords<0)
            {
                throw new IllegalArgumentException("Illegal numWords value: numWords must be greater than -1.\n" + printManual()); 
            }
            
            File myInputFile = new File(args[2]);
            File myOutputFile = new File(args[3]);
            Scanner scan1 = new Scanner(myInputFile);
            PrintWriter write = new PrintWriter(myOutputFile);
            
            RandomTextGenerator randomTextGen = new RandomTextGenerator(scan1, prefixLenght, debug);
            randomTextGen.generateText(numWords , write);
            scan1.close();
            write.close();
        }
        catch(NumberFormatException e)
        {
            System.out.print("prefixLenght and numWords must be Integer arguments "+ printManual() );
        }
        catch(IllegalArgumentException e)
        {
            System.out.println(e.getMessage());
        }
        catch(FileNotFoundException e)
        {
            System.out.println(e.getMessage());
        } 
    }
    
    /** Return a String containing the instructions to run the program. 
     * Printed when the user give incorrect command line arguments. 
    @return the string to display
    */
    private static String printManual()
    {
        return "\n-----------------------------HELP----------------------------\n"
                + "Use the following command to run the program:\n"
                + "java GenText -d prefixLength numWords sourceFile outFile\n"
                + "   1) -d is an optional argument to turn on debug mode"
                + "\n   2) prefixLength must be an integer greater than 0 "
                + "\n   3) numWords must be an integer greater than 1"
                + "\n   4) sourceFile: must be a String with the name of the sourceFile"
                + "\n   5) outFile: must be a String with the name of the outputFile"
                + "\n-------------------------------------------------------------\n";
    }
}





