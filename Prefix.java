// Name:Alejandro Monroy Reyes
// USC loginid: monroyre
// CS 455 PA4
// Fall 2016

import java.util.LinkedList;
import java.util.ListIterator;

/**
Prefix class
*One Prefix
*This class is immutable.
*/

public class Prefix {
    
    private LinkedList<String> wordPrefix;
    
    /**
    Create the prefix;
    @param prefix LinkedList containing n number of words.
    */
    public Prefix(LinkedList<String> prefix){
        this.wordPrefix = prefix;
    }
    
    /**
    Shift the prefix. 
    @param prefix containing words.
    @param word to be added to the prefix. 
    @return new prefix = prefix after removing the last word
                        and inserted new word at the beginning.
     */
    public Prefix shift(Prefix prefix, String word)
    {
        LinkedList<String> newWordPrefix = new LinkedList<String>();
        newWordPrefix = (LinkedList<String>) prefix.wordPrefix.clone();
        newWordPrefix.addLast(word);
        newWordPrefix.removeFirst();    
        return  new Prefix(newWordPrefix);      
    }
    
    /**
    @return the number of words in the Prefix.
     */
    public int getSize()
    {
        return wordPrefix.size();
    }
    
    /**
    @return the hashCode of the prefix.
     */
    public int hashCode()
    {
        return wordPrefix.hashCode();
    }
    
    /**
    @return true if both Prefix are equal.
     */
    public boolean equals(Object o)
    {
     Prefix other = (Prefix) o;
     return wordPrefix.equals(other.wordPrefix);
    }
    
    /**
    @return Prefix object as a string.
     */
    public String toString()
    {
        ListIterator<String> iter = wordPrefix.listIterator();
        String result = "";
        while(iter.hasNext())
        {
            result = result+ iter.next() + " " ;
        }
        return result;
    }
}
